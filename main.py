import random
import sys

def binarySearch(arr, l, r, x):
    if r >= l:
        mid = l + (r - l) // 2
        if arr[mid] == x:
            return mid

        elif arr[mid] > x:
            return binarySearch(arr, l, mid-1, x)

        else:
            return binarySearch(arr, mid + 1, r, x)
    else:
        return -1
 

def main(num):
    arr=[0]*100
    for i in range (0,100):
        arr[i]=random.randint(0,100)
    arr=sorted(arr)
    #print(arr)
    res=res=binarySearch(arr,0,len(arr)-1,num)
    if res==-1:
        print("Number "+str(num)+" not found")
    else:
        print("Number "+str(num)+" found at "+str(res))

if __name__ == "__main__":
    num = int(sys.argv[1])
    main(num)
